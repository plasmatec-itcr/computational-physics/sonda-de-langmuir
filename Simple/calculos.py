'''
En este archivo se almacenan las funciones de cálculos de parámetros de voltaje flotante, corriente de saturación iónica, de la segunda
derivada del voltaje, su estimación en los puntos, el voltaje del plasma por los métodos de segunda derivada, rectas y teórico, y la
temperatura, densidad y corriente de saturación electrónica.
'''

from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
from scipy.optimize import curve_fit

def Calc_Vf(segmentos_df,tol,short=False):

    '''
    Función de cálculo del voltaje flotante

    Lógica: dada una tolerancia, genera un filtro (mask) que evalúa si la corriente se encuentre entre 0-tolerancia
    y 0+tolerancia, lo cual se consideraría que es cercano a 0. Luego se evalúa la corriente con este filtro y lo
    guarda en una lista que son los voltajes (rows) de corriente nula (zero current). En caso de que la lista no esté
    vacía, extrae el índice del voltaje en que la corriente en valor absoluto es mínima (más cercana a cero), para
    luego evaluar el voltaje en ese índice y extraer el voltaje con la corriente más cercana a cero, que sería el
    voltaje flotante. Como la función corre por todos los segmentos, va agregando cada valor de voltaje flotante a
    la lista de voltajes flotantes. Además se le agregó una parámetro en caso de contar con muchos segmentos para que
    no imprima por separado cada vf, sino al final devolver toda la lista.

    Inputs: lista de segmentos, la tolerancia y el parámetro de salidas reducidas.

    Output: una lista con los valores de vf para cada segmento.
    '''

    lista_vf = []

    for i, segmento in enumerate(segmentos_df):
        # Filtrar los valores donde la corriente es 0 o cercana a 0
        zero_current_mask = (segmento['Corriente'] >= -tol) & (segmento['Corriente'] <= tol)
        zero_current_rows = segmento[zero_current_mask]


        if not zero_current_rows.empty:
            # Tomar el voltaje donde la corriente es más cercana a 0
            vf_index = zero_current_rows['Corriente'].abs().idxmin()
            vf_value = zero_current_rows.loc[vf_index, 'Voltaje']
            lista_vf.append(vf_value)
            if short==False:
                print(f"Vf para el segmento {i+1}: {vf_value}")
        else:
            print(f"No se encontró un voltaje flotante para el segmento {i+1}")

    return lista_vf

def Calc_Iis(segmentos_df,min_datos,v_sup,inicio,short=False):

    '''
    Función para calcular la corriente de saturación iónica

    Lógica: Primero filtra los valores negativos nada más, esto porque se asume que la corriente iónica solamente se
    extraerá con un voltaje negativo. Asegurándose que el rango no está vacío, se crea un modelo de regresión lineal
    y se obtiene el voltaje de corte dado por la función fit_ion_sat. Luego filtra nuevamente los datos desde el inicio
    hasta el voltaje de corte. En caso que hayan suficientes datos en el set, se entrena el modelo con los datos de
    voltaje y corriente, y se obtiene un estimador para cuando el voltaje es nulo (intersección de I con V) así como
    su coeficiente de correlación (r^2) y se guarda el valor. Si el parámetro short está desactivado se imprimen uno
    por uno los valores de corriente con su r^2 y el rango de voltaje.

    Inputs: lista de segmentos, el mínimo de datos permitido para entrenar el modelo, el voltaje máximo superior, el
    voltaje de inicio (por ejemplo, -20) y el parámetro de brevedad.

    Output: lista de voltajes flotantes.

    '''
    try:
        lista_Iis = []  # Lista para almacenar las corrientes de saturación iónica de cada segmento
        for i, segmento in enumerate(segmentos_df):
            if short == False:
                print(f"Segmento {i + 1}:")

            # Filtrar solo los valores de Voltaje negativos en el segmento
            segmento_negativo = segmento[segmento['Voltaje'] < 0]

            # Verificar si hay al menos un punto de datos negativo para entrenar el modelo
            if not segmento_negativo.empty:
                # Crear el objeto del modelo de regresión lineal
                model = LinearRegression()

                # Obtiene el punto de corte por medio de fit_ion_sat
                indice_menor_a, a_opt, b_opt, voltaje_corte = fit_ion_sat(segmentos_df,i,v_sup,inicio)

                print(voltaje_corte)
                # Filtrar el segmento solo con los valores de voltaje menores o iguales al voltaje de corte
                segmento_cortado = segmento[segmento['Voltaje'] <= voltaje_corte]
                print(len(segmento_cortado))
                # Verificar si hay al menos n puntos de datos negativos para entrenar el modelo con el segmento cortado
                if len(segmento_cortado) >= min_datos:
                    # Entrenar el modelo de regresión lineal con los datos del segmento cortado
                    model.fit(segmento_cortado[['Voltaje']], segmento_cortado['Corriente'])

                    # Obtener el valor estimado de Corriente cuando Voltaje es igual a 0 con el segmento cortado
                    Iis_cortado = model.predict([[0]])[0]

                    # Calcular el coeficiente de determinación (r^2) con el segmento cortado
                    r_squared = model.score(segmento_cortado[['Voltaje']], segmento_cortado['Corriente'])

                    # Guardar el valor de Iis_cortado en lista_Iis
                    lista_Iis.append(Iis_cortado)

                    if short == False:
                        # Imprimir los resultados para el segmento actual
                        print(f"Corriente de saturación iónica (Iis) del segmento {i + 1}: {Iis_cortado}")
                        print(f"Coeficiente de determinación (r^2) del segmento {i + 1}: {r_squared}")
                        print(f"Rango de voltaje utilizado en el segmento {i + 1}: {segmento_cortado['Voltaje'].min()} - {segmento_cortado['Voltaje'].max()}")
                        print("----------------------")

                else:
                    print("El segmento cortado no contiene suficientes valores de corriente negativos para entrenar el modelo.")
                    print("----------------------")

            else:
                print("No se encontraron valores de corriente negativos para este segmento.")
                print("----------------------")
    except TypeError as e:
            # Maneja específicamente el error de longitud de entrada y salida
        if "Improper input: func input vector length N=2 must not exceed func output vector length M=1" in str(e):
            print("El segmento no es calculable")

    return lista_Iis

def SegundaDerivada(V, I):
    '''
    Función para calcular la segunda derivada utilizando la diferencia finita centrada.

    Inputs: voltaje y corriente.

    Output: segunda derivada de la corriente respecto al voltaje.
    '''
    dv = np.mean(np.diff(V))
    d2I_dV2 = np.gradient(np.gradient(I, dv), dv)

    return d2I_dV2

def Estimar_Vp_Segunda_Derivada(V, I):

    '''
    Función para encontrar el voltaje en que la segunda derivada es cero.

    Inputs: voltaje y corriente.

    Output: voltaje del plasma 
    '''

    d2I_dV2 = SegundaDerivada(V, I)

    # Encontrar el índice donde la segunda derivada es aproximadamente cero
    idx_Vp = np.argmin(np.abs(d2I_dV2))

    # Obtener el voltaje del plasma
    Vp = V[idx_Vp]

    return Vp

def Calc_Vp_SD(segmentos_df,short=False):

    '''
    Función para calcular el voltaje del plasma de cada segmento.

    Lógica: Agarra solo la parte positiva del voltaje y en caso que no esté vacía aplica Estimar_Vp_Segunda_Derivada
    con los valores de voltaje y corriente filtrados.

    Inputs: la lista de segmentos y el parámetro de brevedad.

    Output: lista de voltajes del plasma por segunda derivada.
    '''

    lista_vp_sd = []
    for i, segmento in enumerate(segmentos_df):
        # Filtrar solo los valores de Voltaje y Corriente positivos en el segmento
        segmento_positivo = segmento[segmento['Corriente'] > 0]
        
        if not segmento_positivo.empty:
            # Extraer los valores de voltaje y corriente del segmento positivo
            voltaje = segmento_positivo['Voltaje'].values
            corriente = segmento_positivo['Corriente'].values

            # Calcular el voltaje del plasma utilizando el método de la segunda derivada
            voltaje_plasma = Estimar_Vp_Segunda_Derivada(voltaje, corriente)

            # Guardar los valores de vp en lista_vp_sd
            lista_vp_sd.append(voltaje_plasma)

            if short==False:
                # Imprimir el resultado para el segmento actual
                print(f"Resultados del segmento {i + 1}:")
                print(f"Voltaje del plasma (Vp): {voltaje_plasma}")
                print("----------------------")

        else:
            print(f"No se encontraron valores de corriente positivos para el segmento {i + 1}.")
            print("----------------------")
    return lista_vp_sd

def Calc_Vp_Rectas(segmentos_df, lista_vf, cortes_recta_exponencial, cortes_recta_saturacion, interp_points):

    '''
    Función en desuso para el cálculo del voltaje del plasma. No se recomienda usarla puesto que depende del juicio
    del usuario y no da resultados consistentes.
    '''

    lista_vp_rectas = []
    for i, segmento in enumerate(segmentos_df):
        print(f"Segmento {i+1}:")

        v_f = lista_vf[i]

        # Filtrar los valores de la columna "Corriente" a partir de v_f (o 0 si v_f es negativo)
        if v_f < 0:
            segmento_filtrado = segmento[segmento['Voltaje'] > 0].copy()
        else:
            segmento_filtrado = segmento[segmento['Voltaje'] >= v_f].copy()
        
        if not segmento_filtrado.empty:
            # Calcular el logaritmo natural de la columna "Corriente" en el segmento filtrado
            segmento_filtrado['Logaritmo Corriente'] = np.log(segmento_filtrado['Corriente'])

            # Interpolar más puntos entre los datos existentes
            interp_x = np.linspace(segmento_filtrado['Voltaje'].min(), segmento_filtrado['Voltaje'].max(), interp_points)
            interp_y = np.interp(interp_x, segmento_filtrado['Voltaje'], segmento_filtrado['Logaritmo Corriente'])

            # Realizar la gráfica de los puntos interpolados
            plt.figure(figsize=(10, 6))
            plt.scatter(interp_x, interp_y, color='red', label="Puntos Interpolados", marker='o')
            plt.xlabel('Voltaje')
            plt.ylabel('Logaritmo Natural de Corriente')
            plt.title(f'Gráfica de Logaritmo Natural de Corriente vs. Voltaje (Segmento {i+1})')
            
            # Solicitar al usuario el voltaje del plasma v_p para el ajuste lineal
            corte_recta_exponencial = cortes_recta_exponencial[i]
            
            # Realizar la regresión lineal entre v_f y corte_recta_exponencial
            if v_f < 0:
                mask_exp = (interp_x > 0) & (interp_x <= corte_recta_exponencial)
            else:
                mask_exp = (interp_x >= v_f) & (interp_x <= corte_recta_exponencial)
            slope_exp, intercept_exp = np.polyfit(interp_x[mask_exp], interp_y[mask_exp], 1)
            reg_line_exp = slope_exp * interp_x + intercept_exp
            
            # Graficar la recta de ajuste entre v_f y corte_recta_exponencial
            plt.plot(interp_x, reg_line_exp, color='blue', label="Ajuste Lineal (v_f - Corte Recta Exponencial)")

            corte_recta_saturacion = cortes_recta_saturacion[i]

            # Realizar la regresión lineal entre corte_recta_saturacion y el final
            mask_sat = (interp_x >= corte_recta_saturacion) & (interp_x <= interp_x.max())
            slope_sat, intercept_sat = np.polyfit(interp_x[mask_sat], interp_y[mask_sat], 1)
            reg_line_sat = slope_sat * interp_x + intercept_sat
            
            # Graficar la recta de ajuste entre v_p y v_f
            plt.plot(interp_x, reg_line_sat, color='orange', label="Ajuste Lineal (v_p - Corte Recta Saturacion)")
            
            plt.legend()
            plt.grid(True)
            plt.show()

            # Encontrar el voltaje correspondiente a la intersección entre las rectas

            v_p = (intercept_sat - intercept_exp) / (slope_exp - slope_sat)
            lista_vp_rectas.append(v_p)
            print(v_p)

        else:
            print("El segmento filtrado está vacío.")
        
        print("----------------------")
    return lista_vp_rectas

def Calc_Vp_Teorico(lista_TeV,lista_vf):
    
    '''
    Función para aproximar el voltaje del plasma por medio de una ecuación teórica.

    Lógica: No hay mucha, solamente se asegura de que las unidades sean consistentes. Note que la función depende
    de la temperatura electrónica, la cual a su vez depende del voltaje del plasma, por lo que esta no se puede
    usar para calcular la temperatura electrónica, sino más como un método de validación secundario.

    Inputs: listas de temperatura y voltaje flotante.

    Output: lista de voltaje del plasma.
    '''

    electron = 1.60217646e-19 # Carga del electrón en C
    mi = 1.67262158e-27 # Masa del protón en kg
    me = 9.10938188e-31 # Masa del electrón en kg
    raiz = np.sqrt((2*np.pi*me)/mi) # Raíz cuadrada de la fracción
    ln = np.log(0.61*raiz) # Logaritmo natural de 0.61
    eV_a_J = 1.60217646e-19 # Conversión de eV a J

    lista_vp_teorico = []
    contador = 0
    for TeV in lista_TeV:
        vp = lista_vf[contador] - (TeV*eV_a_J)/electron*ln
        lista_vp_teorico.append(vp)
        contador += 1

    return lista_vp_teorico

def Calc_TeV_Ies_ne(segmentos_df, lista_vf, lista_vp, short=False):
    
    '''
    Función principal para el cálculo de la temperatura (TeV), corriente de saturación (Ies) y densidad electrónica (ne).

    Lógica: Se generan las listas de TeV, Ies y ne, así como del coeficiente de correlación r^2. Para cada segmento,
    se extrae el voltaje flotante y el voltaje del plasma. Primero se filtra el segmento entre el voltaje flotante y
    el del plasma en caso que vf sea positivo, y entre 0 y el voltaje del plasma en caso contrario. Se hace la distinción
    debido a que hay que linealizar la expresión exponencial de la corriente, y no existe definición para el logaritmo
    natural de un valor negativo. Luego se calcula una nueva columna al df correspondiente al logaritmo natural de la
    corriente y se hace una regresión lineal.
    - La temperatura electrónica se define como el inverso de la pendiente de la sección exponencial linealizada.
    Para la corriente de saturación y densidad electrónica se definen los valores de carga elemental en C y de la masa
    del electrón en kg.
    - La corriente de saturación electrónica será aquella en el valor del voltaje del plasma, por lo que se busca el
    valor de voltaje más cercano al voltaje del plasma y se extrae su corriente.
    - Para la densidad electrónica se emplea una ecuación teórica [Falta referenciar].
    En caso que por alguna razón la pendiente o el valor de voltaje cercano al vp no sea posible calcularlo, se toma
    el promedio de los valores calculados más cercanos. Este acercamiento no es del todo correcto, pero por ahora
    funciona como una solución de emergencia. En caso de encontrar una solución más apropiada, contactar al correo
    jsanchezitcr@proton.me.

    Inputs: la lista de segmentos, la lista de valores de voltaje flotante y voltaje del plasma, y el parámetro de
    brevedad.

    Output: las listas de TeV, Ies, ne y los coeficientes de correlación.
    '''

    lista_TeV = []
    lista_Ies = []
    lista_ne = []
    lista_r2 = []

    for i, segmento in enumerate(segmentos_df):
        if short==False:
            print(f"Segmento {i+1}:")
        
        v_f = lista_vf[i]
        v_p = lista_vp[i]
        
        # Filtrar los valores de la columna "Corriente" dentro del intervalo de voltaje
        if v_f<0:
            segmento_filtrado = segmento[(segmento['Voltaje'] >= 0) & (segmento['Voltaje'] <= v_p)].copy()
        else:
            segmento_filtrado = segmento[(segmento['Voltaje'] >= v_f) & (segmento['Voltaje'] <= v_p)].copy()
        
        if not segmento_filtrado.empty:
            # Calcular el logaritmo natural de la columna "Corriente" en el segmento filtrado
            segmento_filtrado.loc[:, 'Logaritmo Corriente'] = np.log(segmento_filtrado['Corriente'])

            # Realizar la regresión lineal entre "Voltaje" y "Logaritmo Corriente"
            slope, intercept, r_value, _, _ = linregress(segmento_filtrado['Voltaje'], segmento_filtrado['Logaritmo Corriente'])

            # Calcular el valor de TeV como 1/m
            TeV = 1 / slope

            # Calcular ne y Ies Revisar página 4 del Chen
            electron_charge = 1.602e-19  # Carga del electrón en C
            electron_mass = 9.109e-31  # Masa del electrón en kg

            # Encontrar la corriente en el voltaje más cercano al voltaje del plasma
            closest_voltage_index = segmento_filtrado['Voltaje'].sub(v_p).abs().idxmin()
            closest_current_to_vp = segmento_filtrado.loc[closest_voltage_index, 'Corriente']

            Ies = closest_current_to_vp

            Ap = 2*np.pi*0.5e-3*3e-3 + np.pi*(0.5e-3)**2  # Área total cilíndrica de la sonda
            ne = closest_current_to_vp / (electron_charge * Ap * np.sqrt(electron_charge * TeV / (2*np.pi*electron_mass)))

            # Guardar los valores
            lista_TeV.append(TeV)
            lista_Ies.append(Ies)
            lista_ne.append(ne)
            lista_r2.append(r_value)
            
            if short==False:
                # Imprimir los resultados para el segmento actual
                print(f"r2 (Coeficiente de determinación): {r_value**2}")
                print(f"TeV: {TeV}")
                print(f"ne: {format(ne,'.5E')}")
                print(f"Ies: {Ies}")
        else:
            print("El segmento filtrado está vacío.")
        if short==False:
            print("----------------------")

    # En caso de aparecer un nan por imposibilidad de cálculo de la derivada, se remplaza por el promedio de los otros dos más cercanos
    for i in range(len(lista_TeV)):
        if np.isnan(lista_TeV[i]):
            previous_value = lista_TeV[i - 1] if i > 0 else lista_TeV[i + 1]
            next_value = lista_TeV[i + 1] if i < len(lista_TeV) - 1 else lista_TeV[i - 1]
            lista_TeV[i] = np.nanmean([previous_value, next_value])
    
    for i in range(len(lista_ne)):
        if np.isnan(lista_ne[i]):
            previous_value = lista_ne[i - 1] if i > 0 else lista_ne[i + 1]
            next_value = lista_ne[i + 1] if i < len(lista_ne) - 1 else lista_ne[i - 1]
            lista_ne[i] = np.nanmean([previous_value, next_value])

    for i in range(len(lista_Ies)):
        if np.isnan(lista_Ies[i]):
            previous_value = lista_Ies[i - 1] if i > 0 else lista_Ies[i + 1]
            next_value = lista_Ies[i + 1] if i < len(lista_Ies) - 1 else lista_Ies[i - 1]
            lista_Ies[i] = np.nanmean([previous_value, next_value])

    return lista_TeV,lista_Ies,lista_ne,lista_r2

def fit_ion_sat(segmentos_df,n_segmento,v_sup,inicio):

    '''
    Esta función sirve para calcular los coeficientes de la recta de mejor ajuste, así como el rango óptimo
    de voltaje.

    Lógica: Primero se extraen los valores de voltaje y corriente, luego se selecciona el índice más cercano al voltaje
    de inicio y lo mismo para el voltaje de corte. Luego se inicializan las listas de errores. Se define una función
    objetivo, que es aquella que tiene la forma a la que vamos a optimizar, en este caso una recta, por lo que los
    parámetros a optimizar serán a (pendiente) y b (intercepto). Ahora se empieza un loop que irá seleccionando un
    rango progresivamente creciente, desde el inicio hasta el corte, haciendo el fit en cada uno y extrayendo la matriz
    de covarianza, que por definición, la raíz cuadrada de su diagonal será el error de ese fit. Se agrega ese error
    a la lista y los valores de a y b optimizados. Además, se extraen los errores de a y b (como son optimizados también
    tienen un error asociado). En este caso se prioriza el error de la pendiente, dado que esta estará relacionada a qué
    tan rápido crece la recta, pero bien podría hacerse tomando el índice de menor error en b. Con este índice se extrae
    el valor de voltaje de corte asociado a este error, y se devuelve junto al índice y los valores optimizados de a y
    b en caso de querer recrear la función de ajuste analíticamente.

    Inputs: la lista de segmentos, el número del segmento a analizar, el voltaje superior de barrido y el de inicio.
    Valores típicos para estos pueden ser v_sup = 0 e inicio = -20 o -15.

    Outputs: el índice de menor error de a, los valores optimizados de a y b (no simultaneamente), y el valor de corte
    del voltaje. Esta función está pensada a remplazar el juicio humano en decidir el punto de corte para el ajuste.
    '''

    # Extrae los datos de Voltaje y Corriente del segmento seleccionado
    datos = segmentos_df[n_segmento-1]
    x_values = np.array(datos["Voltaje"])
    y_values = np.array(datos["Corriente"])

    # Selecciona el índice inicial
    inicio = min(x_values,key=lambda x: abs(x-inicio))
    indice_inicio = np.where(x_values==inicio)[0][0]

    # Selecciona el índice de corte
    corte = min(x_values,key=lambda x: abs(x-v_sup))
    indice_corte_sup = np.where(x_values==corte)[0][0]
    
    # Inicializa una lista para almacenar los errores
    lista_errores = []
    lista_indices = []
    lista_opt = []

    # Define la función objetivo de la cual se intentará aproximar a y b
    def objective(x,a,b):
        return a*x+b
    
    # Inicia un loop en el que barre desde el inicio hasta el corte
    for i in range(indice_inicio,indice_corte_sup+1):
        
        # Hace un slice de las listas para agarrar solo el rango de interés
        x = x_values[:i+1]
        y = y_values[:i+1]

        # Hace el fit, de donde salen dos matrices: la de los valores optimizados, y la de covarianza
        popt, pcov = curve_fit(objective, x, y)
        # De la matriz de covarianza saca la raíz cuadrada de la diagonal
        p_sigma = np.sqrt(np.diag(pcov))
        # Anexa la tupla de error [error_a error_b] a la lista de errores y el índice
        lista_errores.append(p_sigma)
        lista_indices.append(i)
        lista_opt.append(popt)

    # Extrae los errores de a y de b
    errores_a = []
    for i in range(len(lista_errores)):
        errores_a.append(lista_errores[i][0])
    errores_b = []
    for i in range(len(lista_errores)):
        errores_b.append(lista_errores[i][1])
    
    # Extrae el menor error de cada uno
    menor_error_a = min(errores_a)
    menor_error_b = min(errores_b)

    # Saca el índice del punto de corte correcto
    indice_menor_a = lista_indices[errores_a.index(menor_error_a)]

    # Saca los valores óptimos de a y b
    a_opt = lista_opt[errores_a.index(menor_error_a)][0]
    b_opt = lista_opt[errores_a.index(menor_error_a)][1]

    v_corte = x_values[indice_menor_a]

    return indice_menor_a, a_opt, b_opt, v_corte